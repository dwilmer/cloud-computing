package core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import job.JobManager;

public class Monitor extends Thread {
	private String masterHost;
	private int masterPort;
	private String workerHost;
	private int workerPort;
	
	public Monitor(String masterHost, int masterPort, String workerHost, int workerPort) {
		this.masterHost = masterHost;
		this.masterPort = masterPort;
		this.workerHost = workerHost;
		this.workerPort = workerPort;
	}
	
	public void run() {
		Socket connection = null;
		while(true) {
			try {
				// try to connect
				connection = new Socket(masterHost, masterPort);
			} catch(IOException iox) {
				// failed connecting
				System.out.println("Failed connecting to " + masterHost + ":" + masterPort + ". Retrying...");
				
			}
			if(connection != null) {
				try {
					this.monitor(connection);
				} catch(IOException iox) {
					System.out.println("Connection lost. Trying to reconnect...");
				}
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	
	private void monitor(Socket connection) throws IOException {
		BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		BufferedWriter output = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
		
		// send hello
		output.write(workerHost + "\n");
		output.write(workerPort + "\n");
		output.flush();
		
		while(true) {
			String line = input.readLine();
			if(line == null) {
				throw new IOException("Connection closed");
			}
			
			// find things to respond to
			if("load".equals(line)) {
				output.write(Integer.toString(JobManager.getInstance().getLoad()) + "\n");
				output.flush();
			}
			if("marco".equals(line)) {
				output.write("polo\n");
				output.flush();
			}
			if("shutdown".equals(line)) {
				if (JobManager.getInstance().getLoad() > 0) {
					output.write("denied\n");
					output.flush();
				} else {
					try {
						new ProcessBuilder("shutdown", "-P", "1").start(); // Failures happen here
						output.write("ok\n");
						output.flush();
						Runtime.getRuntime().exit(0);
					} catch (IOException ioe) {
						output.write("failed\n");
						output.flush();
					}
				}
			}
		}
	}
}
