package core;

import http.Server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Main {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length < 2) {
			System.out.println("Error! Not enough arguments! Need at least two arguments: master host and port. Optional: listen port.");
		}
		String masterHost = args[0];
		int masterPort = 0;
		try {
			masterPort = Integer.parseInt(args[1]);
			assert masterPort > 0;
			assert masterPort < 65536;
		} catch (NumberFormatException e) {
			notAValidPortNumber();
		} catch (AssertionError e) {
			notAValidPortNumber();
		}
		
		
		int port = 80;
		if(args.length > 2) {
			try {
				port = Integer.parseInt(args[2]);
				assert port > 0;
				assert port < 65536;
			} catch (NumberFormatException e) {
				notAValidPortNumber();
			} catch (AssertionError e) {
				notAValidPortNumber();
			}
		}
		
		Monitor mon = new Monitor(masterHost, masterPort, getIpAddress(), port);
		mon.start();
		
		Server server = new Server(port, new InputHandler());
		server.start();
	}
	
	private static void notAValidPortNumber() {
		System.out.println("Error! Argument is not a valid port number. Please provide an integer between 0 and 65536 (both exclusive).");
		System.exit(1);
	}
	
	private static String getIpAddress() {
		try {
			Socket connection = new Socket("automation.whatismyip.com", 80);
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
			
			out.write("GET /n09230945.asp HTTP/1.1\r\nHost: automation.whatismyip.com\r\nConnection: close\r\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0\r\n\r\n");
			out.flush();
			String line = in.readLine();
			
			// read headers
			while(line.length() > 1) {
				line = in.readLine();
			}
			while(line.length() <= 1) {
				line = in.readLine();
			}
			String myAddress = line;
			connection.close();
			
			return myAddress;
		} catch (IOException e) {
		}
		return null;
	}
	
}
