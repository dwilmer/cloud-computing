package imageMagick;


public class ImageMagickException extends Exception {
    public ImageMagickException(Throwable cause) {
        super(cause);
    }
}