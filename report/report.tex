\documentclass[11pt]{article}
\usepackage{hyperref}
\usepackage{fullpage}

\bibliographystyle{amsplain}

\author{Alexander van Gessel\\a.p.h.vangessel@student.tudelft.nl \and Daan Wilmer \\ d.w.h.wilmer@student.tudelft.nl}
\title{Cloud Computing: Large Exercise}

\begin{document}
\maketitle

\begin{abstract}
	To provide an image manipulation web service, we developed a robust system using a master-worker architecture that automatically scales up using the Amazon Elastic Compute Cloud (EC2).
	This systems scales fully automated, requiring no maintenance whatsoever --- apart from the unlikely event that the master crashes, in which case it can be restarted for the system to become fully operational again.
\end{abstract}

\section{Introduction}
Many online services have some sort of image manipulation.
Images on Wikipedia, for example, are stored in several different sizes.
Because image manipulation can be costly, we developed a web service to do that for them.
And, as demand can rise and rop, we used the Amazon cloud to scale our application following demand.

There are online services that can resize and manipulate images.
However, we do not know of any open-source web services or whether they can automatically scale using the cloud.

In the remainder of the article we will first go into the application in more details in section \ref{sec:application}, after that we will describe the design of the system in section \ref{sec:design}.
Experiments will then be explained in section \ref{sec:experiments}, after which we will discuss the system and experiments in section \ref{sec:discussion} and conclude in section \ref{sec:conclusion}.

\section{Background on Application}
\label{sec:application}
The application we implemented is a web interface for cropping, resizing and liquid rescaling (also known as seam carving)\cite{seamCarving} of images.
Handling images is done by ImageMagick, while the rest of the system was built in Java.
The system uses the Amazon Elastic Compute Cloud to request more resources whenever necessary.
In order for this to work, credentials must be supplied for logging into the Amazon client, and for logging into new servers.

Our system runs on Java, so that is a requirement, and there is an init script which installs ImageMagick.
We therefore need some way to install ImageMagick, usually done using a package manager like `apt'.
If the target system does not have `apt', then the init script must be adapted to change the way ImageMagick is installed.
The version of ImageMagick must be at least 6.3.8, to support liquid rescaling.

Requirements in terms of memory are very small. Java needs only a couple MB of memory, as does ImageMagick. Even the EC2 micro instances provide plenty of room with 600MB of RAM.
The application does require a considerable amount of processing power to save request files and for liquid rescaling. Cropping and resizing require just a couple of seconds to finish.

\section{System Design}
\label{sec:design}
The system consists of one master node and several worker nodes.
Both master and worker node set up an HTTP server to handle job requests.

The master node responds to requests for /status with an overview of workers and their work load, while any other request will result in a HTTP 307 redirect to one of the workers. The master stops reading HTTP requests after the headers, so its bandwidth is not the bottleneck for jobs submitted to it.

Worker nodes can display their jobs, their job status and the input and output files for each job.
Also, using a form, new jobs can be submitted to the worker.

\subsection{Resource Management Architecture}
The master node keeps track of all worker nodes.
To distribute the work load as fair as possible, the master regularly polls the workers for their current load (the number of running jobs) and assigns new jobs to workers with the lowest work load at the time.
This overview of workers with their work load is available at the /status page.

This regular polling also allows the master node to keep track of dead and overused workers.
The master node can then remove dead workers from the list or request more instances from the Amazon EC2 cloud if necessary.
If too many worker nodes are idling (no load), the master node can instruct them to shut down.
This will result in a terminated instance, which the master will first try to restart if more resources are required.

There is currently no automatic failover mechanism for the master node.
However, if the master process is restarted at the same location (hostname and port) of the old master process, all workers automatically connect to the new server instance.

\subsection{System Policies}
We have several policies that determine the behaviour of our system.

\begin{description}
	\item[Worker selection] Of all workers, the worker with the lowest current load is selected. If there are several workers with the same load, one of them is randomly chosen.
	\item[Worker update] The load of all workers is requested every second. If the connection fails, the worker is removed from the master.
	\item[Master reconnection] Workers that lose connection to the master try to reconnect every five seconds.
	\item[Scaling up] Experiments showed that when a worker has two jobs running it already becomes significantly slower.
		Our current policy is to request a new instance when the minimum load reaches two.
	\item[Scaling down] When more than one worker is idle, the master will stop one of the idle instances if that instance has been idle for at least sixty seconds.
		This ensures that there are not many workers idling, while clients have opportunity to retrieve the result.
	\item[Failover] When a job fails because the worker fails, the job can be resubmitted to the master. It will then assign the job to a new worker.
\end{description}

\section{Experimental results}
\label{sec:experiments}
In order to test our application, we ran several experiments.
These experiments were performed during the development, to test wheter newly implemented features functioned well, and afterwards, to test the system's performance.

Our experiments were run on the Amazon EC2 cloud, using t1.micro instances with ebs storage.
Using ebs enabled us to stop the instances and then restart them without having to install Java, ImageMagick or MagickCloud again.

To conduct experiments, we submitted jobs to the system, either automated via cURL\footnote{cURL is an application to submit HTTP requests from a command line interface, making it easy to automate.} or manually through the web form.
Jobs that were submitted automatically, were sent to the master and then redirected.
Manually uploaded jobs were manually distributed over the workers to create a desired load.

The jobs we submitted were all liquid-rescale jobs of large images (1920x1080), because this type of job is by far the most compute-intensive job.
Resizing and cropping images are done in a few seconds, while liquid-rescaling can take up to several minutes, depending on the image and available computing power.

Monitoring was done through the status page of the master, which gives an overview of connected workers and their number of running jobs, and using the Amazon AWS console, which shows the instances and their state.

\subsection{Experiments}
In this part we will discuss the specific experiments and our observations. Discussion and conclusion of the results will be in sections \ref{sec:discussion} and \ref{sec:conclusion}, respectively.

\subsubsection{Worker failover}
Using the AWS management console we stopped or terminated one of the worker instances.
Monitoring the status pages showed that this worker's death was detected and the worker removed from the list of available workers within a second.
This ensures that it, if clients are redirected to a worker that dies before the job is finished, the clients can resubmit their job after a second to get redirect to a good worker.

\subsubsection{Master failover}
Using ssh to login to the master instance, we stopped the master process.
This meant that the status page became unavailable and that new jobs could no longer be submitted.
Jobs that were already running, however, continued regularly.
Moreover, when the master process was brought back up on the same location (the same hostname and port) as the first process, the workers automatically reconnected.

\subsubsection{No workers available}
For this experiment we ran the master with no workers running and no stopped instances.
Using the AWS console we saw that a new instance was being requested and started, and some time later we saw that it connected to the master and accepted jobs.
The whole process from requesting an instance to the worker process running on an instance is not very fast: it takes several minutes to get a new instance up and running.

\subsubsection{No workers available - take 2}
Because of the results from the previous experiment we implemented the reuse of previous installations.
To do this we stopped (not terminated) a fully running worker instance before starting the master.
As we started the master we saw that, instead of requesting a new istance, the stopped instances was restarted.
Although it was significantly faster, this approach still takes well over a minute before the worker becomes available.

\subsubsection{Peak load}
To investigate how well the system scales up under load, and scales down afterwards, we submitted 12 jobs after eachother to the system using cURL.
On beforehand the system has no load at all, with only a single worker with zero running jobs.

After the first two jobs were submitted to the worker, the system started an extra instance.
As soon as this worker was online, the worker started assigning jobs to it until the work was evenly balanced.
When the load on both workers exceeded two, one more instance was started up.
Unfortunately, by the time this instance was up and running, all jobs were already assigned to the first two workers.
This worker then sat idling until one of the other workers finished all its jobs, and then it got told to shut down.
After both workers were done with their jobs one of them was succesfully shut down by the master.

\section{Discussion}
\label{sec:discussion}
The application that is currently built is quite simple, and cannot automatically recover from master failure.
Job rescheduling is also missing, as is recovery from failure. Currently the only solution is to resubmit the job.

The policies we implemented are also quite simple.
They were first chosen based upon our own (limited) experience, only to be changed when experiments showed really bad performance.
However, we feel that they can be fine tuned to very well suit a specific type of work load.

Lastly, the supported operations for image manipulations are still very limited.
Due to time pressure we decided to stick to this small subset of manipulations, to keep the functionality as simple as possible.
In the future this could be easily expanded to allow a wider variety of jobs.

\section{Conclusion}
\label{sec:conclusion}
The application we developed is a powerful tool, wich can be extended to provide advanced image manipulation on a scalable set of machines.
Policies may also be adapted to fit the expected work load and requirements.

In any case: it takes quite long to request and set up new servers from Amazon (several minutes).
Scaling up fast therefore is not possible, and the application scales best when the work load increases and decreases gradually.

We do recommend, however, to have one or two servers not on the amazon cloud.
Since the system requires at least one master and one worker, is is much cheaper to rent one or two (virtual) servers somewhere else permanently.
The power of amazon is the easy scaling, so the system can be used for scaling up when necessary.

\section*{Appendix A: Time Sheets}
\begin{tabular}[b]{l | c c c c c c | c}
Day & Think & Dev & XP & Analysis & Write & Wasted & Total \\
\hline
\hline
Tuesday October 30 2012   & 3 & 0  & 0 & 0 & 1 & 8 & 12 \\
Wednesday October 31 2012 & 4 & 8  & 0 & 0 & 0 & 0 & 12 \\
Thursday November 1 2012  & 2 & 2  & 0 & 0 & 0 & 2 & 6 \\
Friday November 2 2012    & 0 & 4  & 0 & 0 & 0 & 0 & 4 \\
Tuesday November 6 2012   & 1 & 9  & 0 & 0 & 0 & 0 & 10 \\
Wednesday November 7 2012 & 2 & 16 & 0 & 0 & 0 & 0 & 18\\
Thursday November 8 2012  & 3 & 6  & 3 & 2 & 0 & 0 & 14\\
Friday November 9 2012    & 2 & 10 & 7 & 7 & 2 & 0 & 28\\
Saturday November 10 2012 & 0 & 0  & 0 & 0 & 3 & 0 & 3 \\
Sunday November 11 2012   & 0 & 0  & 0 & 0 & 4 & 0 & 4 \\
\hline
Total & 17 & 55 & 10 & 9 & 10 & 10 & 111
\end{tabular}

\bibliography{report}
\end{document}
