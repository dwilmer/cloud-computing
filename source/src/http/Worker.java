package http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Worker extends Thread {
	private Socket connection;
	private RequestHandler handler;
	private boolean truncate;
	
	public Worker(Socket socket, RequestHandler handler, boolean truncate) {
		this.connection = socket;
		this.handler = handler;
		this.truncate = truncate;
	}
	
	public void run() {
		try {
			InputStream input = this.connection.getInputStream();
			OutputStream output = this.connection.getOutputStream();
			Response response = this.handler.handleRequest(new Request(input, truncate));
			response.writeTo(output);
			connection.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
