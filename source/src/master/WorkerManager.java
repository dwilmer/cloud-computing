package master;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class WorkerManager {
	public static int LOAD_THRESHOLD = 2;
	private static WorkerManager instance;
	
	private Collection<Worker> workers;

	private enum SuggestedAction {
		DONOTHING,
		CREATE,
		DESTROY;
	}
	
	public static WorkerManager getInstance() {
		if(instance == null) {
			instance = new WorkerManager();
		}
		return instance;
	}

	private WorkerManager() {
		this.workers = Collections.synchronizedList(new ArrayList<Worker>());
		
		new Thread(new Runnable() {	
			private long lastModify;
			private final long action_interval_ms = 60000;
			@Override
			public void run() {
				this.lastModify = System.currentTimeMillis();
				while(true) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
					try {
						SuggestedAction suggestion = WorkerManager.getInstance().pingWorkers();
						if (suggestion != SuggestedAction.DONOTHING
							&& System.currentTimeMillis() > this.lastModify + action_interval_ms) {
							if (suggestion == SuggestedAction.CREATE) {
								new Thread(new Runnable() {
									
									@Override
									public void run() {
										AWS.initNewAmazonInstance();
									}
								}).start();
								this.lastModify = System.currentTimeMillis();
							} else { // DESTROY
								Worker victim = WorkerManager.getInstance().getBestWorker();
								if (victim.getLastChange() > action_interval_ms + System.currentTimeMillis())
									if (victim.shutdown())
										this.lastModify = System.currentTimeMillis();
							}
						}
					} catch(Throwable t) {
						t.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public void addWorker(Worker w) {
		this.workers.add(w);
	}
	
	protected SuggestedAction pingWorkers() {
		int minLoad = Integer.MAX_VALUE;
		int idle = 0;
		List<Worker> newlist = Collections.synchronizedList(new ArrayList<Worker>());
		for(Worker w : this.workers) {
			try {
				w.refreshLoad();
				if(w.isAlive()) {
					newlist.add(w);
					if(w.getLoad() < minLoad) {
						minLoad = w.getLoad();
					}
					if(w.getLoad() == 0) {
						idle++;
					}
				}
			} catch(IOException iox) {
			}
		}
		this.workers = newlist;
		if(minLoad >= LOAD_THRESHOLD)
			return SuggestedAction.CREATE;
		else if(idle > 1)
			return SuggestedAction.DESTROY;
		else
			return SuggestedAction.DONOTHING;
	}
	
	public void listen(final int port){
		new Thread(new Runnable() {
			@Override
			public void run() {
				ServerSocket server;
				try {
					server = new ServerSocket(port);
					while(true) {
						Socket connection = server.accept();
						BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
						String address = in.readLine();
						int workerPort = Integer.parseInt(in.readLine());
						System.out.println("Heard worker: " + address + ":" + workerPort);
						WorkerManager.getInstance().addWorker(new Worker(connection, address, workerPort));
					}
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(2);
				}
			}
		}).start();
	}

	public Worker getBestWorker() {
		List<Worker> best = new java.util.ArrayList<Worker>();
		int lowestLoad = Integer.MAX_VALUE;
		
		for(Worker w : this.workers) {
			if(!w.isAlive()) {
			} else if(w.getLoad() < lowestLoad) {
				lowestLoad = w.getLoad();
				best.clear();
				best.add(w);
			} else if(w.getLoad() == lowestLoad) {
				best.add(w);
			}
		}
		if (best.isEmpty())
			return null;
		return best.get(new java.util.Random().nextInt(best.size()));
	}
	
	public Collection<Worker> getWorkers() {
		return this.workers;
	}
}
