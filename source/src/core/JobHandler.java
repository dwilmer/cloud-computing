package core;

import http.Request;
import http.Request.Method;
import http.Response;

import job.Job;
import job.Job.FileType;
import job.Job.JobStatus;
import job.JobManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collection;

public class JobHandler extends BaseHandler {
	private Request request;
	public JobHandler(Request request) {
		this.request = request;
	}
	public Response handle() throws IOException {
		String url = this.request.getUrl();
		if("/job".equals(url)) {
			return noJobID();
		}
		if(url.matches("^/job/[0-9]+$")) {
			int jobId = Integer.parseInt(url.substring(5));
			Job job = JobManager.getInstance().getJob(jobId);
			if (job != null)
				return this.jobRequest(job);
			else if (jobId < Job.getLastId())
				return this.gone();
			else
				return this.unknownPage(url);
		}
		if(url.matches("^/job/[0-9]+/image$")) {
			int jobId = Integer.parseInt(url.substring(5, url.length() - 6));
			Job job = JobManager.getInstance().getJob(jobId);
			if(job != null && job.getStatus() == JobStatus.FINISHED) {
				return sendOutfile(job);
			} else if (job != null) {
				return waitLonger();
			}
		}
		if(url.matches("^/job/[0-9]+/original$")) {
			int jobId = Integer.parseInt(url.substring(5, url.length() - 9));
			Job job = JobManager.getInstance().getJob(jobId);
			if(job != null) {
				return sendInfile(job);
			}
		}
		return this.unknownPage(url);
	}

	private Response noJobID() {
		Response response = checkMethod(this.request.getMethod(), java.util.Arrays.asList(Method.POST, Method.GET));
		if (response != null)
			return response;

		if (this.request.getMethod() == Method.POST)
			return newJob();
		else // GET
			return getJobs();
	}

	private Response newJob() {
		String filename = this.request.getParam("img");
		String op_param = this.request.getParam("convert");
		Job.JobType operation = Job.JobType.getJobType(op_param);
		Integer width = null;
		Integer height = null;
		try {
			width = new Integer(this.request.getParam("width"));
		} catch (NumberFormatException nfe) {}
		try {
			height = new Integer(this.request.getParam("height"));
		} catch (NumberFormatException nfe) {}
		FileType filetype = FileType.JPEG;
		if("png".equals(this.request.getParam("extension"))) {
			filetype = FileType.PNG;
		}
		Job job = JobManager.getInstance().newJob(operation, filename, width, height, filetype);
		Response response = new Response(Response.STATUS_SEE_OTHER);
		response.setHeader("Location", "/job/" + job.getJobId());
		return response;
	}

	private Response getJobs() {
		Collection<Job> jobs = JobManager.getInstance().getJobs();

		Response response = new Response(Response.STATUS_OK);
		response.setHeader("Content-Type", "text/html");
		StringBuilder responseBuilder = new StringBuilder();
		responseBuilder.append("<!doctype html><html><head><title>Jobs</title></head><body><h1>Jobs</h1>");
		responseBuilder.append("We currently have " + Integer.toString(jobs.size()) + " jobs<br />");
		for (Job job: jobs) {
			String s = Integer.toString(job.getJobId());
			responseBuilder.append("<p><a href=\"/job/" + s + "\">Job " + s + "</a></p>");
		}
		responseBuilder.append("<br /><a href=\"/form\">Create a new job</a></body></html>");

		response.setRawContent(responseBuilder.toString());

		return response;
	}

	private Response jobRequest(Job job) {
		Response response = checkMethod(this.request.getMethod(), java.util.Arrays.asList(Method.GET, Method.DELETE, Method.PATCH));
		if (response != null)
			return response;

		if (this.request.getMethod() == Method.GET)
			return jobStatus(job);
		else if (this.request.getMethod() == Method.DELETE)
			return jobDelete(job);
		else if (this.request.getMethod() == Method.PATCH && this.request.getParam("action").toLowerCase().equals("kill"))
			return jobKill(job);
		else
			return notImplemented();
	}
	private Response jobStatus(Job job) {
		Response response;
		if(job == null) {
			response = new Response(Response.STATUS_NOTFOUND);
			response.setHeader("Content-Type", "text/html");
			response.setRawContent("<!doctype html><html><head><title>Not found</title></head><body><h1>Not found!</h1><p>The requested job could not be found.</p><p><a href=\"/\">Return to home</a></p></body></html>");
		} else {
			response = new Response(Response.STATUS_OK);

			response.setHeader("Content-Type", "text/html");
			if (job.getStatus() == JobStatus.RUNNING)
				response.setHeader("Refresh", "5");

			StringBuilder responseBuilder = new StringBuilder();
			responseBuilder.append("<!doctype html><html><head><title>Job ");
			responseBuilder.append(job.getJobId());
			responseBuilder.append("</title></head><body><h1>Job ");
			responseBuilder.append(job.getJobId());
			responseBuilder.append("</h1><p>Status: ");
			responseBuilder.append(job.getStatus().toString());
			if(job.getStatus() == JobStatus.FINISHED) {
				responseBuilder.append("<br /><a href=\"/job/");
				responseBuilder.append(job.getJobId());
				responseBuilder.append("/image\">Download</a>");
			}
			responseBuilder.append("<br /><a href=\"/job/");
			responseBuilder.append(job.getJobId());
			responseBuilder.append("/original\">Original image</a><br />");
			if(job.getStatus() == JobStatus.RUNNING) {
				responseBuilder.append("<form action=\"/job/");
				responseBuilder.append(job.getJobId());
				responseBuilder.append("\" method=\"POST\"><input type=\"hidden\" name=\"method\" value=\"patch\" /><input type=\"submit\" name=\"action\" value=\"Kill\" /></form>");
			}
			responseBuilder.append("<form action=\"/job/");
			responseBuilder.append(job.getJobId());
			responseBuilder.append("\" method=\"POST\"><input type=\"hidden\" name=\"method\" value=\"delete\" /><input type=\"submit\" value=\"Delete\" /></form>");
			responseBuilder.append("</p></body></html>");
			response.setRawContent(responseBuilder.toString());
		}

		return response;
	}
	private Response jobDelete(Job job) {
		JobManager.getInstance().deleteJob(job.getJobId());

		Response response = new Response(Response.STATUS_OK);

		response.setHeader("Content-Type", "text/plain");
		response.setRawContent("Deleted");

		return response;
	}
	private Response jobKill(Job job) {
		Response response;
		if (job.getStatus() == JobStatus.FINISHED || job.getStatus() == JobStatus.ERROR) {
			response = new Response(Response.STATUS_OK);
			response.setRawContent("Already stopped");
		} else {
			job.kill();

			response = new Response(Response.STATUS_OK);
			response.setRawContent("Stopped");
		}
		response.setHeader("Content-Type", "text/plain");

		return response;
	}

	private Response sendOutfile(Job job) throws IOException {
		Response response = checkMethod(this.request.getMethod(), java.util.Arrays.asList(Method.GET));
		if (response != null)
			return response;
		return sendImage(job.getOutfile(), job.getFileType());
	}

	private Response sendInfile(Job job) throws IOException {
		Response response = checkMethod(this.request.getMethod(), java.util.Arrays.asList(Method.GET));
		if (response != null)
			return response;
		FileType filetype = job.getInputFileType();
		File infile = job.getInfile();
		return sendImage(infile, filetype);
	}

	private Response sendImage(File file, FileType filetype) throws IOException {
		Response response = new Response(Response.STATUS_OK);

		long filesize = file.length();
		ByteBuffer buf = ByteBuffer.allocate((int)filesize);
		InputStream is = new FileInputStream(file);
		is.read(buf.array());

		response.setHeader("Content-Type", filetype.getMimeType());
		response.setHeader("Content-Disposition", "inline");
		response.setHeader("Content-Length", Long.toString(filesize));
		response.setRawContent(buf.array());

		return response;
	}

	private Response waitLonger() {
		Response response = new Response(Response.STATUS_ENHANCE_YOUR_CALM);

		response.setHeader("Content-Type", "text/plain");
		response.setHeader("Refresh", "5");

		response.setRawContent("Image not ready yet");

		return response;
	}

	private Response gone() {
		Response response = new Response(Response.STATUS_GONE);

		response.setHeader("Content-Type", "text/plain");

		response.setRawContent("Gone");

		return response;
	}
	private Response notImplemented() {
		Response response = new Response(Response.STATUS_NOT_IMPLEMENTED);

		response.setHeader("Content-Type", "text/plain");

		response.setRawContent("Not Implemented");

		return response;
	}
}
