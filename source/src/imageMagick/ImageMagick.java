package imageMagick;

import org.im4java.core.*;

/**
*
* Class to provide interface with ImageMagick
* 
*/
public class ImageMagick {
    public static void main(String[] args) throws java.io.IOException, ImageMagickException {
        new ImageMagick().liquidRescale(args[0], args[1], new Integer(300), (Integer)null, false);
    }
    protected void run_operation(Operation op, boolean async) throws java.io.IOException, ImageMagickException {
        ConvertCmd cmd = new ConvertCmd();
        cmd.setAsyncMode(async);
        try {
            cmd.run(op);
        } catch (InterruptedException e) {
            throw new ImageMagickException(e);
        } catch (IM4JavaException e) {
            throw new ImageMagickException(e);
        }
    }
    public void resize(String infile, String outfile, Integer width, Integer height, boolean async) throws java.io.IOException, ImageMagickException {
        IMOperation op = new IMOperation();
        op.addImage(infile);
        op.resize(width, height);
        op.addImage(outfile);
        run_operation(op, async);
    }
    public void crop(String infile, String outfile, Integer width, Integer height, boolean async) throws java.io.IOException, ImageMagickException {
        IMOperation op = new IMOperation();
        op.addImage(infile);
        op.gravity("center");
        op.crop(width, height, 0, 0);
        op.addImage(outfile);
        run_operation(op, async);
    }
    public void liquidRescale(String infile, String outfile, Integer width, Integer height, boolean async) throws java.io.IOException, ImageMagickException {
        IMOperation op = new IMOperation();
        op.addImage(infile);
        op.liquidRescale(width, height);
        op.addImage(outfile);
        run_operation(op, async);
    }
}
