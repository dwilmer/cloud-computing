package core;

import http.Request;
import http.Response;

class UnknownHandler extends BaseHandler {
	private Request request;
	public UnknownHandler(Request request) {
		this.request = request;
	}
	public Response handle() {
		return this.unknownPage(request.getUrl());
	}
}

public class HandlerFactory {
	public static BaseHandler getHandler(Request request) {
		if (request.getUrl().indexOf("/job") == 0) {
			return new JobHandler(request);
		} else {
			return new UnknownHandler(request);
		}
	}
}
