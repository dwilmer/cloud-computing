package job;

import imageMagick.ImageMagick;
import imageMagick.ImageMagickException;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class Job extends Thread {
	public enum JobType {
		RESIZE("resize"),
		CROP("crop"),
		LIQUID_RESCALE("liquid-rescale");

		private String name;
		private JobType(String name) {
			this.name = name;
		}
		public static JobType getJobType(String name) {
			for (JobType jt :JobType.values())
				if (name.equals(jt.name))
					return jt;
			throw new UnsupportedOperationException("Unknown JobType");
		}
	}
	
	public enum FileType {
		JPEG("jpeg"),
		PNG("png");

		private String abbrev;
		private FileType(String abbrev) {
			this.abbrev = abbrev;
		}
		public String getExtension() {
			return "." + abbrev;
		}
		
		public String getMimeType() {
			return "image/" + abbrev;
		}
	}
	
	public enum JobStatus {
		READY, RUNNING, ERROR, FINISHED
	}
	
	private static AtomicInteger nextJobID = new AtomicInteger();
	
	private int id;
	private JobType type;
	private int height;
	private int width;
	private File infile;
	private File outfile;
	private FileType filetype;
	private JobStatus status;
	private Exception exception;

	private static File indir = new File("/tmp/infiles");
	private static File outdir = new File("/tmp/outfiles");
	private static void makeDirs() {
		if (!indir.exists() && !(indir.mkdir() || indir.exists()))
			throw new RuntimeException("Could not create infile directory!");
		if (!outdir.exists() && !(outdir.mkdir() || outdir.exists()))
			throw new RuntimeException("Could not create outfile directory!");
	}
	
	public Job(JobType type, String tmpfile, int height, int width, FileType ft) {
		this.id = nextJobID.getAndIncrement();
		this.type = type;
		this.height = height;
		this.width = width;
		this.status = JobStatus.READY;
		this.filetype = ft;
		makeDirs();
		this.infile = new File(indir, "image" + this.id + getFileTypeByExt(new File(tmpfile)).getExtension());
		if (!new File(tmpfile).renameTo(infile))
			throw new RuntimeException("Could not move tempfile to infile directory!");
		this.outfile = new File(outdir, "image" + this.id + ft.getExtension());
		
		this.exception = null;
	}

	public void run() {
		JobManager.getInstance().incLoad();
		this.status = JobStatus.RUNNING;
		try {
			ImageMagick im = new ImageMagick();
			String in = this.infile.getCanonicalPath();
			String out = this.outfile.getCanonicalPath();
			switch(this.type) {
				case RESIZE:
					im.resize(in, out, width, height, false);
					break;
				case CROP:
					im.crop(in, out, width, height, false);
					break;
				case LIQUID_RESCALE:
					im.liquidRescale(in, out, width, height, false);
					break;
			}
			this.status = JobStatus.FINISHED;
		} catch(ImageMagickException imx) {
			this.status = JobStatus.ERROR;
			this.exception = imx;
		} catch (IOException iox) {
			this.status = JobStatus.ERROR;
			this.exception = iox;
		} finally {
			JobManager.getInstance().decLoad();
		}
	}
	
	public int getJobId() {
		return this.id;
	}
	
	public File getInfile() {
		return this.infile;
	}

	public File getOutfile() {
		return this.outfile;
	}
	
	public JobStatus getStatus() {
		return this.status;
	}
	
	public FileType getFileType() {
		return this.filetype;
	}
	public Exception getException() {
		return this.exception;
	}

	public void kill() {
		while (this.status != JobStatus.FINISHED && this.status != JobStatus.ERROR) {
			this.interrupt();
			Thread.yield();
		}
	}
	public void delete() {
		this.kill();
		this.getInfile().delete();
		this.getOutfile().delete();
	}

	public static int getLastId() {
		return nextJobID.get();
	}

	public FileType getInputFileType() {
		return getFileTypeByExt(this.infile);
	}
	private static FileType getFileTypeByExt(File file) {
		FileType filetype = FileType.JPEG;
		String filename = file.getName().toLowerCase();
		for (FileType ft: FileType.values()) {
			String typeext = ft.getExtension().toLowerCase();
			if (filename.length() >= typeext.length() && typeext.equals(filename.substring(filename.length() - typeext.length())))
				filetype = ft;
		}
		return filetype;
	}
}
