package master;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusResult;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateName;
import com.amazonaws.services.ec2.model.InstanceStatus;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;


public class AWS {
	private static AmazonEC2Client client;
	private static Map<String, String> settings;
	private static boolean requestPending = false;
	
	private static void readSettings() throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader("ec2config"));
		settings = new HashMap<String, String>(5);
		
		String line = reader.readLine();
		while(line != null && !"".equals(line)) {
			String[] tokens = line.split("=", 2);
			settings.put(tokens[0], tokens[1]);
			line = reader.readLine();
		}
		reader.close();
	}
	
	private static AmazonEC2Client getAmazonClient() {
		if(client == null) {
			try {
				readSettings();
				
				AWSCredentials credentials = new BasicAWSCredentials(settings.get("accessKeyId"), settings.get("accessKey"));
				client = new AmazonEC2Client(credentials);
				client.setEndpoint(settings.get("endpoint"));
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException npx) {
				System.out.println("Failed to get EC2 settings");
			}
		}
		return client;
	}
	
	private static Instance startNewAmazonInstance() {
		AmazonEC2Client client = getAmazonClient();
		if(client == null) {
			System.out.println("Failed to get settings");
		} else {
			RunInstancesRequest request = new RunInstancesRequest();
			request.withImageId(settings.get("imageId"))
				.withInstanceType(InstanceType.T1Micro)
				.withMinCount(1)
				.withMaxCount(1)
				.withKeyName(settings.get("keyName"))
				.withSecurityGroups(settings.get("securityGroup"));
			
			RunInstancesResult result = client.runInstances(request);
			
			List<Instance> instances = result.getReservation().getInstances();
			if(instances.size() > 0) {
				Instance instance = instances.get(0);
				
				return getRunningInstance(instance.getInstanceId());
			}
		}
		return null;
	}

	private static Instance getRunningInstance(String instanceId) {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
		}
		
		InstanceStatus status = getInstanceStatus(instanceId);
		String instanceStateName = "unknown";
		if(status != null) {
			instanceStateName = status.getInstanceState().getName();
		}
		while(status == null || !instanceStateName.equals(InstanceStateName.Running.toString())) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
			}
			status = getInstanceStatus(instanceId);
			if(status != null) {
				instanceStateName = status.getInstanceState().getName();
			}
		}
		return getInstance(instanceId);
	}
	
	private static InstanceStatus getInstanceStatus(String instanceId) {
		AmazonEC2Client client = getAmazonClient();
		DescribeInstanceStatusRequest request = new DescribeInstanceStatusRequest()
			.withInstanceIds(instanceId);
		DescribeInstanceStatusResult result = client.describeInstanceStatus(request);
		List<InstanceStatus> statuses = result.getInstanceStatuses();
		if(statuses.size() > 0) {
			return statuses.get(0);
		}
		return null;
	}
	
	private static Instance getInstance(String instanceId) {
		AmazonEC2Client client = getAmazonClient();
		DescribeInstancesRequest request = new DescribeInstancesRequest()
			.withInstanceIds(instanceId);
		DescribeInstancesResult result = client.describeInstances(request);
		List<Reservation> reservations = result.getReservations();
		if(reservations.size() > 0) {
			List<Instance> instances = reservations.get(0).getInstances();
			if(instances.size() > 0) {
				return instances.get(0);
			}
		}
		return null;
	}
	
	private static Instance restartStoppedInstance() {
		AmazonEC2Client client = getAmazonClient();
		if(client == null) {
			return null;
		}
		DescribeInstancesRequest request = new DescribeInstancesRequest()
			.withFilters(new Filter("instance-state-name").withValues(InstanceStateName.Stopped.toString()));
		DescribeInstancesResult result = client.describeInstances(request);
		List<Reservation> reservations = result.getReservations();
		if(reservations.size() > 0) {
			List<Instance> instances = reservations.get(0).getInstances();
			if(instances.size() > 0) {
				Instance instance = instances.get(0);
				
				StartInstancesRequest startRequest = new StartInstancesRequest()
					.withInstanceIds(instance.getInstanceId());
				client.startInstances(startRequest);
				
				return getRunningInstance(instance.getInstanceId());
			}
		}
		return null;
	}
	
	public static void initNewAmazonInstance() {
		if(requestPending) {
			return;
		}
		requestPending = true;
		
		// try getting stopped instance
		Instance instance = restartStoppedInstance();
		if(instance == null) {
			instance = requestNewAmazonInstance();
		}
		if(instance != null) {
			// start magickCloud
			String address = instance.getPublicIpAddress();
			String masterAddress = Main.getMasterAddress();
			int masterPort = Main.getMasterPort();

			int attemptsLeft = 10;
			boolean success = false;
			while(attemptsLeft > 0 && !success) {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
				}
				
				try {
					Socket test = new Socket(instance.getPublicDnsName(), 22);
					success = test.isConnected();
				} catch(IOException iox) {
					System.out.println("Failed connecting, retrying...");
				}
				attemptsLeft--;
			}
			
			if(success) {
				try {
					String sshCommand = "ssh -v ubuntu@" + address + " -i identityfile.pem -o StrictHostKeyChecking=no";
					Process p = Runtime.getRuntime().exec(sshCommand);
					BufferedWriter sshWriter = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
					sshWriter.write("./init.sh\n");
					sshWriter.write("sudo java -jar magickcloud.jar ");
					sshWriter.write(masterAddress + " " + masterPort + " 2>>magic.log &\n");
					sshWriter.write("exit\n");
					sshWriter.write("\u0004");
					sshWriter.flush();
					p.waitFor();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		requestPending = false;
	}
	
	private static Instance requestNewAmazonInstance() {
		// start new instance
		Instance instance = startNewAmazonInstance();
		
		String address = instance.getPublicIpAddress();
		
		int attemptsLeft = 10;
		boolean success = false;
		while(attemptsLeft > 0 && !success) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
			}
			
			try {
				Socket test = new Socket(instance.getPublicDnsName(), 22);
				success = test.isConnected();
			} catch(IOException iox) {
				System.out.println("Failed connecting, retrying...");
			}
			attemptsLeft--;
		}
		if(success) {
			System.out.println("Installing new instance...");
			Runtime runtime = Runtime.getRuntime();
			try {
				// copy init.sh
				String[] copyCommand = {"scp", "-r", "-o StrictHostKeyChecking=no", "-o IdentityFile=/home/ubuntu/identityfile.pem", "init.sh", "ubuntu@" + address + ":init.sh"};
				Process p = runtime.exec(copyCommand);
				p.waitFor();
				
				// copy jarfile
				copyCommand[4] = "magickcloud.jar";
				copyCommand[5] = "ubuntu@" + address + ":magickcloud.jar";
				p = runtime.exec(copyCommand);
				p.waitFor();
				
				// copy im4java jarfile
				copyCommand[4] = "magickcloud_lib";
				copyCommand[5] = "ubuntu@" + address + ":magickcloud_lib";
				p = runtime.exec(copyCommand);
				p.waitFor();
				
				// run init and jar file
				String sshCommand = "ssh -v ubuntu@" + address + " -i identityfile.pem";
				p = runtime.exec(sshCommand);
				final BufferedReader sshReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
				final BufferedReader ssherrorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							String line = sshReader.readLine();
							while(line != null) {
								System.out.println("> " + line);
								line = sshReader.readLine();
							}
						} catch(IOException iox) {
							iox.printStackTrace();
						}
					}
				}).start();
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							String line = ssherrorReader.readLine();
							while(line != null) {
								System.out.println("! " + line);
								line = ssherrorReader.readLine();
							}
						} catch(IOException iox) {
							iox.printStackTrace();
						}
					}
				}).start();
				BufferedWriter sshWriter = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
				sshWriter.write("./init.sh\n");
				sshWriter.write("exit\n");
				sshWriter.write("\u0004");
				sshWriter.flush();
				p.waitFor();
				
				return instance;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
