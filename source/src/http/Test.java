package http;

@Deprecated
public class Test implements RequestHandler {

	@Override
	public Response handleRequest(Request request) {
		Response response = new Response(Response.STATUS_NOTFOUND);
		if(!"/favicon.ico".equals(request.getUrl())) {
			response = new Response(Response.STATUS_OK);
			response.setHeader("Content-Type", "text/html");
			if("/form".equals(request.getUrl())) {
				response.setRawContent("<!doctype html><html><head><title>Test!</title></head><body><h1>Form</h1><form enctype=\"multipart/form-data\" method=\"post\" action=\"handler\"><input name=\"file\" type=\"file\" /><input type=\"submit\" /></form></html>");
			} else {
				response.setRawContent("<!doctype html><html><head><title>Test!</title></head><body><h1>Test!</h1><p>Hello from " + request.getUrl() + ".</p></html>");
			}
			
		}
		return response;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int port = 1337;
		if(args.length > 0) {
			try {
				port = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
			}
		}
		Server server = new Server(port, new Test());
		server.start();
	}

}
