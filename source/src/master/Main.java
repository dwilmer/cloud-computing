package master;

import http.Request;
import http.RequestHandler;
import http.Response;
import http.Server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Collection;

public class Main {
	private static String masterAddress;
	private static int masterPort;
	
	public static void main(String[] args) {
		masterPort = 1500;
		if(args.length > 0) {
			try {
				masterPort = Integer.parseInt(args[0]);
				assert masterPort > 0;
				assert masterPort < 65536;
			} catch (NumberFormatException e) {
				notAValidPortNumber();
			} catch (AssertionError e) {
				notAValidPortNumber();
			}
		}
		
		int jobPort = 80;
		if(args.length > 1) {
			try {
				jobPort = Integer.parseInt(args[1]);
				assert jobPort > 0;
				assert jobPort < 65536;
			} catch (NumberFormatException e) {
				notAValidPortNumber();
			} catch (AssertionError e) {
				notAValidPortNumber();
			}
		}
		
		WorkerManager.getInstance().listen(masterPort);
		
		masterAddress = getIpAddress();
		
		new Server(jobPort, new RequestHandler() {
			@Override
			public Response handleRequest(Request request) {
				if("/status".equals(request.getUrl())) {
					return statusResponse();
				} else {
					return forwardResponse(request);
				}
			}
			
			private Response statusResponse() {
				Response response = new Response(Response.STATUS_OK);
				StringBuilder responseBuilder = new StringBuilder();
				responseBuilder.append("<!doctype html><html><head><title>Worker status</title></head><body><h1>Worker status</h1>");
				Collection<Worker> workers = WorkerManager.getInstance().getWorkers();
				if(workers.isEmpty()) {
					responseBuilder.append("<p>No workers.</p>");
				} else {
					responseBuilder.append("<table><tr><th>Worker</th><th>Load</th></tr>");
					for(Worker w : workers) {
						responseBuilder.append("<tr><td><a href=\"");
						responseBuilder.append(w.getRootUrl());
						responseBuilder.append("\">");
						responseBuilder.append(w.getRootUrl());
						responseBuilder.append("</a></td><td>");
						responseBuilder.append(w.getLoad());
						responseBuilder.append("</td></tr>");
					}
					responseBuilder.append("</table>");
				}
				responseBuilder.append("</body></html>");
				response.setRawContent(responseBuilder.toString());
				return response;
			}
			
			private Response forwardResponse(Request request) {
				Worker bestWorker= WorkerManager.getInstance().getBestWorker();
				Response response;
				if(bestWorker == null) {
					// oops
					response = new Response(Response.STATUS_INTERNAL_ERROR);
					response.setRawContent("<!doctype html><html><head><title>Internal error</title></head><body><p>No workers found</p></body></html>");
				} else {
					// bye bye
					response = new Response(Response.STATUS_TEMPORARY_REDIRECT);
					response.setHeader("Location", bestWorker.getRootUrl() + request.getUrl().substring(1));
				}
				return response;
			}
		}, true).start();
	}
	
	public static String getMasterAddress() {
		return masterAddress;
	}
	
	public static int getMasterPort() {
		return masterPort;
	}
	
	private static void notAValidPortNumber() {
		System.out.println("Error! One of the arguments is not a valid port number. Please provide an integer between 0 and 65536 (both exclusive).");
		System.exit(1);
	}
	
	private static String getIpAddress() {
		try {
			Socket connection = new Socket("automation.whatismyip.com", 80);
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
			
			out.write("GET /n09230945.asp HTTP/1.1\r\nHost: automation.whatismyip.com\r\nConnection: close\r\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0\r\n\r\n");
			out.flush();
			String line = in.readLine();
			
			// read headers
			while(line.length() > 1) {
				line = in.readLine();
			}
			while(line.length() <= 1) {
				line = in.readLine();
			}
			String myAddress = line;
			connection.close();
			
			return myAddress;
		} catch (IOException e) {
		}
		return null;
	}
}
