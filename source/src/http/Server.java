package http;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 
 * Simple HTTP server to handle requests
 *
 */
public class Server extends Thread{
	public static int HTTP_DEFAULT_PORT = 80;
	private int port;
	private RequestHandler requestHandler;
	private boolean truncate;
	
	public Server(RequestHandler handler) {
		this(HTTP_DEFAULT_PORT, handler);
	}
	public Server(int port, RequestHandler handler) {
        this(port, handler, false);
    }
	public Server(int port, RequestHandler handler, boolean truncate) {
		this.port = port;
		this.requestHandler = handler;
		this.truncate = truncate;
	}
	
	/**
	 * Open a connection, listen, and dispatch workers when necessary
	 */
	public void run() {
		try {
			ServerSocket server = new ServerSocket(port);
			while(true) {
				Socket client = server.accept();
				(new Worker(client, this.requestHandler, this.truncate)).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
