package http;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Request {
	public enum Method {
		GET("GET"),
		HEAD("HEAD"),
		POST("POST"),
		PUT("PUT"),
		DELETE("DELETE"),
		TRACE("TRACE"),
		OPTIONS("OPTIONS"),
		CONNECT("CONNECT"),
		PATCH("PATCH");

		private String name;
		private Method(String name) {
			this.name = name;
		}
		public String toString() {
			return this.name;
		}
	}

	private Method method;
	private String url;
	private Map<String, String> headers;
	private Map<String, String> getRequestParams;
	private Map<String, String> postRequestParams;

	public Request(InputStream inputStream, boolean truncate) throws IOException {
		// setup headers
		this.headers = new HashMap<String, String>();

		// decode request
		String line = readline(inputStream);
		if("".equals(line)) {
			line = readline(inputStream);
		}
		decodeRequest(line);

		// read headers
		line = readline(inputStream);
		while(!"".equals(line)) {
			readHeader(line);
			line = readline(inputStream);
		}

		if (truncate)
			return;

		// read content
		this.readContent(inputStream);

		// Hidden form-field method override
		String method = this.getParam("method");
		// Don't allow the abuse of GET
		if (method != null && this.getMethod() != Method.GET) {
			this.setMethod(method.toUpperCase());
		}
	}

	public Method getMethod() {
		return this.method;
	}

	public String getUrl() {
		return this.url;
	}

	public String getHeader(String key) {
		return this.headers.get(key);
	}

	public String getParam(String name) {
		String param = this.postRequestParams.get(name);
		if(param != null) {
			return param;
		}
		return this.getRequestParams.get(name);
	}

	public String getGetParam(String name) {
		return this.getRequestParams.get(name);
	}

	public String getPostParam(String name) {
		return this.postRequestParams.get(name);
	}

	private void setMethod(String method) {
		this.method = null;
		for (Method m :Method.values()) {
			if (method.equals(m.toString())) {
				this.method = m;
				break;
			}
		}
	}

	private void decodeRequest(String request) {
		// request = first line: <method> <url> HTTP/<version>
		// only method and url are important
		String[] tokens = request.split(" ", 3);

		// get url
		int querystringIndex = tokens[1].indexOf("?");
		if(querystringIndex >= 0) {
			this.url = tokens[1].substring(0, querystringIndex);
			String querystring = tokens[1].substring(querystringIndex + 1);
			this.getRequestParams = decodeQuerystring(querystring);
		} else {
			this.url = tokens[1];
			this.getRequestParams = new HashMap<String, String>(0);
		}

		// get method
		String method = tokens[0];
		this.setMethod(method);
	}

	private void readHeader(String header) {
		String[] tokens = header.split(": ", 2);
		this.headers.put(tokens[0].toLowerCase(), tokens[1]);
	}

	private void readContent(InputStream input) throws IOException {
		if(this.method == Method.POST) {
			String contentType = this.headers.get("content-type");
			if(contentType != null && contentType.indexOf("multipart") == 0) {
				this.postRequestParams = new HashMap<String, String>();

				// find boundary
				int boundaryStart = contentType.indexOf("boundary=") + 9;
				String boundary = "--" + contentType.substring(boundaryStart);

				// read first boundary
				pipeUntilBoundary(input, null, boundary);

				boolean hasMore = true;
				while(hasMore) {
					hasMore = readContentParts(input, boundary);
				}

			} else {
				String contentLengthHeader = this.headers.get("content-length");
				if(contentLengthHeader != null) {
					int length = Integer.parseInt(contentLengthHeader);
					byte[] content = new byte[length];
					input.read(content);
					this.postRequestParams = decodeQuerystring(new String(content));
				} else {
					StringBuilder contentBuilder = new StringBuilder();
					byte[] buf = new byte[1024];
					int numRead = 1024;
					while(numRead == 1024) {
						numRead = input.read(buf);
						if(numRead != -1) {
							contentBuilder.append(new String(buf));
						}
					}
					this.postRequestParams = decodeQuerystring(contentBuilder.toString());
				}
			}
		} else {
			this.postRequestParams = new HashMap<String, String>(0);
		}
	}

	private boolean readContentParts(InputStream input, String boundary) throws IOException {
		// read part header
		String line = readline(input);
		if("--".equals(line)) {
			return false;
		}

		// find name
		Pattern namePattern = Pattern.compile(" name=\"([^\"]*)\"");
		Matcher nameMatcher = namePattern.matcher(line);
		if(nameMatcher.find()) {
			String name = nameMatcher.group(1);

			Pattern filenamePattern = Pattern.compile(" filename=\"([^\"]*)\"");
			Matcher filenameMatcher = filenamePattern.matcher(line);

			while(!"".equals(line)) {
				line = readline(input);
			}

			if(filenameMatcher.find()) {
				File file = new File(filenameMatcher.group(1));
				String filename = "/tmp/" + Integer.toString(new java.util.Random().nextInt()) + file.getName();
				this.postRequestParams.put(name, filename);
				OutputStream out = new FileOutputStream(new File(filename));
				pipeUntilBoundary(input, out, "\r\n" + boundary);
			} else {
				StringBuilder valueBuilder = new StringBuilder();
				boolean firstLine = true;
				line = readline(input);
				while(line.indexOf(boundary) == -1) {
					if(!firstLine) {
						valueBuilder.append('\n');
					}
					valueBuilder.append(line);
					firstLine = false;
					line = readline(input);
				}
				this.postRequestParams.put(name, valueBuilder.toString());
				if(line.equals(boundary + "--")) {
					return false;
				}
				
			}
		}
		return true;
	}

	private void pipeUntilBoundary(InputStream in, OutputStream out, String boundary) throws IOException {
		byte[] buf = new byte[boundary.length()];
		int read = in.read();
		int boundaryIndex = 0;
		while(read != -1 && boundaryIndex < boundary.length()) {
			byte readByte = (byte)read;
			char readChar = (char)read;
			if(readChar == boundary.charAt(boundaryIndex)) {
				buf[boundaryIndex] = readByte;
				boundaryIndex++;
			} else {
				if(out != null) {
					for(int i = 0; i < boundaryIndex; i++) {
						out.write(buf[i]);
					}
					out.write(readByte);
				}
				boundaryIndex = 0;
			}
			if(boundaryIndex < boundary.length()) {
				read = in.read();
			}
		}
	}



	private static String readline(InputStream in) throws IOException {
		StringBuilder lineBuilder = new StringBuilder();
		int c = in.read();
		while(c != -1 && c != 10) {
			if(c != 13) {
				lineBuilder.append((char)c);
			}
			c = in.read();
		}
		return lineBuilder.toString();
	}

	private static Map<String, String> decodeQuerystring(String querystring) {
		Map<String, String> params = new HashMap<String, String>();

		String[] tokens = querystring.split("&");
		for(String token : tokens) {
			int equals = token.indexOf("=");
			if(equals > 0) {
				params.put(token.substring(0, equals), token.substring(equals + 1));
			}
		}

		return params;
	}
}
