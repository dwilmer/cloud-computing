package http;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Response {
	public static String STATUS_OK = "200 OK";
	public static String STATUS_ACCEPTED = "202 ACCEPTED";
	public static String STATUS_NO_CONTENT = "204 NO CONTENT";
	public static String STATUS_SEE_OTHER = "303 SEE OTHER";
	public static String STATUS_TEMPORARY_REDIRECT = "307 TEMPORARY REDIRECT";
	public static String STATUS_NOTFOUND = "404 NOT FOUND";
	public static String STATUS_METHOD_NOT_ALLOWED = "405 METHOD NOT ALLOWED";
	public static String STATUS_GONE = "410 GONE";
	public static String STATUS_ENHANCE_YOUR_CALM = "420 ENHANCE YOUR CALM";
	public static String STATUS_INTERNAL_ERROR = "500 INTERNAL SERVER ERROR";
	public static String STATUS_NOT_IMPLEMENTED = "501 NOT IMPLEMENTED";
	
	private String status;
	private Map<String, String> headers;
	private byte[] rawContent;
	
	public Response(String status) {
		this.status = status;
		this.headers = new HashMap<String, String>();
	}
	
	public void setHeader(String key, String value) {
		this.headers.put(key, value);
	}
	
	public void setRawContent(String rawContent) {
		this.rawContent = rawContent.getBytes();
	}
	
	public void setRawContent(byte[] rawContent) {
		this.rawContent = rawContent;
	}

	public void writeTo(OutputStream output) throws IOException {
		BufferedWriter buf = new BufferedWriter(new OutputStreamWriter(output));
		buf.write("HTTP/1.0 " + this.status + "\r\n");
		for(Entry<String, String> header : headers.entrySet()) {
			buf.write(header.getKey() + ": " + header.getValue() + "\r\n");
		}
		buf.write("\r\n");
		buf.flush();
		
		if(this.rawContent != null) {
			output.write(this.rawContent);
		}
	}
}
