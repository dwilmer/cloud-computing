package core;

import http.Request;
import http.Request.Method;
import http.RequestHandler;
import http.Response;

public class InputHandler implements RequestHandler {
	
	@Override
	public Response handleRequest(Request request) {
		try {
			if("/form".equals(request.getUrl())) {
				return uploadForm();
			} else if("/".equals(request.getUrl())) {
				return hello();
			} else {
				return HandlerFactory.getHandler(request).handle();
			}
		} catch (Throwable t) {
			t.printStackTrace();
			return internalError();
		}
	}
	
	private Response uploadForm() {
		Response response = new Response(Response.STATUS_OK);
		
		response.setHeader("Content-Type", "text/html");
		response.setRawContent("<!doctype html><html><head><title>MagickCloud</title></head><body><h1>Submit job</h1><form enctype=\"multipart/form-data\" action=\"job\" method=\"POST\"><fieldset><legend>Upload image</legend><input type=\"file\" name=\"img\" /></fieldset><fieldset><legend>Manipulation</legend><select name=\"convert\"><option selected>-- select --</option><option value=\"resize\">Resize</option><option value=\"crop\">Crop</option><option value=\"liquid-rescale\">Liquid Rescale</option></select><br /><label for=\"width\">Width:</label><input type=\"text\" name=\"width\" id=\"width\" /><br /><label for=\"height\">Height:</label><input type=\"text\" name=\"height\" id=\"height\" /><br /><label for=\"extension\">Output type</label><select name=\"extension\" id=\"extension\"><option value=\"jpg\" selected>.jpg</option><option value=\"png\">.png</option></select></fieldset><input type=\"submit\" value=\"Submit job\" /></form></body></html>");
		
		return response;
	}
	
	private Response hello() {
		Response response = new Response(Response.STATUS_OK);

		response.setHeader("Content-Type", "text/html");
		response.setRawContent("<!doctype html><html><head><title>MagickCloud</title></head><body><h1>Hello</h1><a href=\"form\">Upload form</a><br /><a href=\"job\">Jobs</a></body></html>");

		return response;
	}

	private Response internalError() {
		Response response = new Response(Response.STATUS_INTERNAL_ERROR);
		response.setHeader("Content-Type", "text/html");
		response.setRawContent("<!doctype html><html><head><title>Internal Server Error</title></head><body><h1>Internal Server Error!</h1><p>Some sort of error occurred.</p><p><a href=\"/\">Return to home</a></p></body></html>");
		return response;
	}
}
