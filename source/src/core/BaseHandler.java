package core;

import http.Request.Method;
import http.Response;

public abstract class BaseHandler {
    public abstract Response handle() throws java.io.IOException;

	protected Response unknownPage(String url) {
		Response response = new Response(Response.STATUS_NOTFOUND);

		response.setHeader("Content-Type", "text/html");
		response.setRawContent("<!doctype html><html><head><title>Not found</title></head><body><h1>Not found!</h1><p>The page you requested, <em>" + url + "</em>, could not be found.</p><p><a href=\"/\">Return to home</a></p></body></html>");

		return response;
	}

	/// Return null if allowed, return a 405 response if not allowed
	protected Response checkMethod(Method method, java.util.Collection<Method> allowed) {
		if (allowed.contains(method))
			return null;
		else
			return methodNotAllowed(allowed);
	}

	protected Response methodNotAllowed(Iterable<Method> allowed_methods) {
		Response response = new Response(Response.STATUS_METHOD_NOT_ALLOWED);
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (Method m :allowed_methods) {
			if (first)
				first = false;
			else
				sb.append(", ");
			sb.append(m.toString());
		}
		response.setHeader("Allow", sb.toString());

		response.setHeader("Content-Type", "text/html");
		response.setRawContent("<!doctype html><html><head><title>Method not allowed</title></head><body><h1>Method not allowed!</h1><p>Valid methods are: " + sb.toString() + "</p><a href=\"/\">Return to home</a></p></body></html>");

		return response;
	}
}
