package job;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import job.Job.FileType;
import job.Job.JobType;

public class JobManager {
	private static JobManager instance;
	
	private Map<Integer, Job> jobs;
	private int load;
	
	public static JobManager getInstance() {
		if(instance == null) {
			instance = new JobManager();
		}
		
		return instance;
	}
	
	private JobManager() {
		this.load = 0;
		this.jobs = new TreeMap<Integer, Job>();
	}
	
	public Job newJob(JobType type, String infile, int height, int width, FileType ext) {
		Job toCreate = new Job(type, infile, height, width, ext);
		this.jobs.put(toCreate.getJobId(), toCreate);
		toCreate.start();
		return toCreate;
	}
	
	public Job getJob(int id) {
		return this.jobs.get(id);
	}

	public Collection<Job> getJobs() {
		return this.jobs.values();
	}

	public void deleteJob(int id) {
		this.jobs.remove(id).delete();
	}
	
	public int getLoad() {
		return this.load;
	}
	
	void incLoad() {
		this.load++;
	}
	
	void decLoad() {
		this.load--;
	}
}
